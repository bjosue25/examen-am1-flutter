import 'package:flutter/material.dart';
import '../constants.dart';
import '../model/basket.dart';

class EmailWidget extends StatelessWidget {
  final Basket basket;
  final Function onTap;


  const EmailWidget(
      {Key? key,
      required this.basket,
      required this.onTap,
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details) {
      },
      onLongPress: () {
      },
      onTap: () {
        onTap(basket);
      },
      
      child: Container(
      
        padding: const EdgeInsets.all(10.0),
        height: 80.0,

        child: Row(
       
          
          children: <Widget> [
            
            
            Expanded(
              flex: 1,
              child: Container(
                height: 12.0,
                
              ),
              
            ),
            Expanded(
              flex: 9,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text('Nombre : ${basket.name}', style: fromTextStyle),
                   Text('Couch :${basket.coach}', style: subjectTextStyle),
                  Text('Region: ${basket.region}',
                      style: fromTextStyle),
                 
                ],
              ),
            )
       
          ],
        ),
      ),
    );
  }
}
