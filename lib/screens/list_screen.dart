import 'package:flutter/material.dart';

import '../constants.dart';
import '../model/backend.dart';
import '../widgets/basket_widget.dart';
import 'detail_screen.dart';
import '../model/basket.dart';

class ListScreen extends StatefulWidget {


  const ListScreen({Key? key, required this.title}) : super(key: key);


  final String title;

  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {


  
  
  var baskets = Backend().getBasket();
 

  void showDetail( Basket basket) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return DetailScreen( basket: basket,);
    }));
    
    setState(() {
 
      baskets = Backend().getBasket();
    });
  }

  @override
  Widget build(BuildContext context) {
 
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title), 
        actions: <Widget>[
        IconButton(
            icon: const Icon(Icons.donut_small), 
            tooltip: 'BASKET',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('NBA')));
            },
          ),
          ]
      
      ),


      
      body: ListView.separated(
        itemCount: baskets.length,
        separatorBuilder: (BuildContext context, int index) => const Divider(
          color: primaryColor,
          indent: 40.0,
          endIndent: 20.0,
        ),
          itemBuilder: (BuildContext context, int index) => EmailWidget(
          basket: baskets[index],
          onTap: showDetail,  
  
        ),
      
          
      ),  

    );  
  }
}
