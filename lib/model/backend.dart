import 'basket.dart';

class Backend {

  static final Backend _backend = Backend._internal();

  factory Backend() {
    return _backend;
  }

  Backend._internal();

  final _baskets=[
     Basket(id: 1, name: 'Cleveland Cavaliers', coach: 'J. B. Bickerstaff', titulo:'7' , region: 'Central ', trofeo: ' El Trofeo Nat “Sweetwater” Clifton'),
      Basket(id: 2, name: 'Los Angeles Lakers', coach: 'Darvin Ham', titulo: '17 ' , region: 'Pacífico',  trofeo: ' El Trofeo Willis Reed'),
      Basket(id: 3, name: 'Golden State Warriors', coach: 'Steve Kerr', titulo: '7' , region: 'Pacífico', trofeo: 'El Trofeo Chuck Cooper'),
      Basket(id: 4, name: 'Miami Heat', coach: ' Erik Spoelstra.', titulo: '3' , region: 'Sureste',  trofeo: 'El Trofeo Earl Lloyd'),
       Basket(id: 5, name: 'Boston Celtics', coach: 'Joe Mazzulla Tendencia.', titulo: '17', region: 'Conferencia Este',  trofeo: 'El Trofeo Sam Jones'),
  ];


 List<Basket> getBasket(){
   return _baskets;
 }
 


}
