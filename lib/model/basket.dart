import 'package:flutter/cupertino.dart';

class Basket {
  final int id;
  final String name;
  final String coach;
  final String titulo;
  final String region;
  final String trofeo;

  Basket({
    required this.id,
    required this.name,
    required this.coach,
    required this.titulo,
    required this.region,
    required this.trofeo,
  });
}

